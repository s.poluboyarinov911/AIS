public class Quad {

    public static double[] quadEq(double a, double b, double c) throws QuadException {

        double D = Math.pow(b, 2) - 4 * a * c;
        double eps = 0.000001;

        if (-eps < a && a < eps) {
            throw new QuadException(QuadErrorCode.FIRST_COEFF_IS_ZERO);
        }
        if (D < 0) {
            throw new QuadException(QuadErrorCode.NO_ROOTS);
        }

        double[] roots = new double[2];
        roots[0] = (-b + Math.sqrt(D)) / (2 * a);
        roots[1] = (-b - Math.sqrt(D)) / (2 * a);
        return roots;
    }
}

