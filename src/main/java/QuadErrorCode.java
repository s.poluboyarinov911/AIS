public enum QuadErrorCode {

    NO_ROOTS("The equation has no roots at all"),
    FIRST_COEFF_IS_ZERO("First coefficient can not be zero");

    private String errorString;

    private QuadErrorCode(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
