public class QuadException extends Exception {

    QuadErrorCode quadEquationErrorCode;

    public QuadException(QuadErrorCode quadEquationErrorCode) {
        super(quadEquationErrorCode.getErrorString());
        this.quadEquationErrorCode = quadEquationErrorCode;
    }

    public QuadErrorCode getErrorCode() {
        return quadEquationErrorCode;
    }
}
