import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestQuad {

    @Test
    void SolveGivesPositiveOnesIsTrue() throws QuadException {
        double[] array = {1, 1};
        assertArrayEquals(array, Quad.quadEq(1, -2, 1));
    }

    @Test
    void SolveGivesPosAndNegOnesIsTrue() throws QuadException {
        double[] array = {1, -1};
        assertArrayEquals(array, Quad.quadEq(1, 0, -1));
    }

    @Test
    void SolveGivesNoRootsException() {
        try {
            Quad.quadEq(1, 0, 1);
        } catch (QuadException ex) {
            assertEquals(QuadErrorCode.NO_ROOTS, ex.getErrorCode());
        }
    }

    @Test
    void SolveGives() {
        try {
            Quad.quadEq(0, 1, 1);
        } catch (QuadException ex) {
            assertEquals(QuadErrorCode.FIRST_COEFF_IS_ZERO, ex.getErrorCode());
        }
    }

}
